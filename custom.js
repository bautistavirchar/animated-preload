$(document).ready(function(){
    var style = '<style> .mystroke { stroke-dasharray: 1200; animation: my_animation 2s cubic-bezier(0, 0.23,1,.1) infinite; }'+
                '@keyframes my_animation { 0% { stroke-dashoffset: 0; }'+
                '30% {stroke:darkgreen;} 50% { stroke-dashoffset: 1200;} 80% {stroke:orangered;}'+
                '100% { stroke-dashoffset: 0; } }'+
                '#lardc-logo { width: 160px; margin-top: -80px; margin-left: -60px;}</style>';
                
    $('head').append(style);
    var logo = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 397.3 322" enable-background="new 0 0 397.3 322" xml:space="preserve">'+
                    '<g id="Layer_1">'+
                        '<polygon class="mystroke" fill="none" stroke="#2F2A75" stroke-width="3" stroke-miterlimit="10" points="333.6,312.2 333.6,223 388.1,223 208.6,9.4 8.8,223 62.8,223 62.8,312.2 	" />'+
                        '<polygon class="mystroke" fill="none" stroke="#2F2A75" stroke-width="3" stroke-miterlimit="10" points="328.5,223 174.7,50.9 132,97.2 247.6,223.5 	" />'+
                        '<rect class="mystroke" x="174" y="226.8" fill="none" stroke="#2F2A75" stroke-width="3" stroke-miterlimit="10" width="73.6" height="79.3" />'+
                    '</g>'+
                '</svg>';
    HoldOn.open({
        theme:"custom",
        content:'<div id="lardc-logo" class="center-block">'+ logo +'</div>',
        message:'<strong class="center-block"><h3>Please wait...</h3></strong>',
        backgroundColor:"white",
        textColor:"#2a1479"
    });
});

$(window).on('load',function () {
    HoldOn.close();
});